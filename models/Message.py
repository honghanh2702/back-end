import datetime
from db import db

class Message:
    def __int__(self,
                _id:str = "",
                IDsend:str ="",
                IDreceive:str = "",
                content:str = "",
                status:str = "",
                created_at:str = str(datetime.datetime.now()),
                updated_at:str = str(datetime.datetime.now())):
        self._id = _id
        self.IDsend = IDsend
        self.IDreceive = IDreceive
        self.content = content
        self.status = status
        self.created_at = created_at
        self.updated_at = updated_at

    @classmethod
    def getMessage(cls,currentUserId,userid):
        listmess = list(db["message"].find({"$or":[{"IDsend":currentUserId,"IDreceive":userid},{"IDsend":userid,"IDreceive":currentUserId}]},{"_id":0}))
        return listmess

    def createMessage(self):
        messObj = {"IDsend":self.IDsend,"IDreceive":self.IDreceive,"content":self.content,"status":self.status,"created_at":self.created_at,"updated_at":self.updated_at}
        db["message"].insert_one(messObj)
        return str(messObj.get("_id",""))