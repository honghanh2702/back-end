from bson import ObjectId
from db import db

class Follow:
    def __int__(self, _id: str = str(ObjectId), followerId: str = "", followedId: str = ""):
        self._id = _id
        self.followerId = followerId
        self.followedId = followedId
    def createOrDeleteFollow(self):
        followObj = list(db["follow"].find({"followerId":self.followedId,"followedId":self.followedId}))
        if len(followObj) == 0:
            followObjCreate = {"followerId":self.followerId,"followedId":self.followedId}
            db["follow"].insert_one(followObjCreate)
            return followObjCreate.get("_id","")
        else:
            db["follow"].delete_one({"followerId":self.followerId,"followedId":self.followedId})
            return "delete success"
    @classmethod
    def isFollow(cls,userId,followedId)->bool:
        followObj = list(db["follow"].find({"followerId": userId, "followedId":followedId}))
        if len(followObj) == 0:
            return True
        else:
            return False